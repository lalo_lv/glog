## golg admin

This is a Web UI to view aggregated cluster stats in realtime and perform various administrative tasks.

Read the docs

## Working Locally

$ npm install
$ ./gulp clean watch or $ ./gulp clean build
$ go-bindata --debug --pkg=nsqadmin --prefix=static/build static/build/...
$ go build && ./nsqadmin
make changes (repeat step 5 if you make changes to any Go code)
$ go-bindata --pkg=nsqadmin --prefix=static/build static/build/...
commit other changes and bindata.go