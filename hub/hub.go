package main

import (
	"context"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/gin-gonic/gin"
	nsq "github.com/nsqio/go-nsq"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	yaml "gopkg.in/yaml.v2"
)

var conf gin.H
var mgoSess *mgo.Session

// 库名
const dbName string = "dcj_common"

func main() {
	// 读取配置文件
	confile := flag.String("c", "config.yaml", "config file path")
	flag.Parse()
	buf, err := ioutil.ReadFile(*confile)
	if err != nil {
		fmt.Fprintf(os.Stderr, "File Error: %s\n", err)
		panic(err.Error())
	}
	// 解析 YAML 格式
	err = yaml.Unmarshal(buf, &conf)
	if err != nil {
		fmt.Println("YAML 解析错误")
		panic(err.Error())
	}

	// 连接数据库
	mgoSess, err = mgo.Dial(conf["mgo"].(string))
	if err != nil {
		fmt.Println("MongoDB连接失败")
		panic(err) // no, not really
	}
	mgoSess.SetMode(mgo.Monotonic, true)
	fmt.Println("Connected to MongoDB!")

	// 调试模式设置
	if conf["debug"].(int) == 1 {
		gin.SetMode(gin.DebugMode)
	} else {
		gin.SetMode(gin.ReleaseMode)
	}

	r := gin.New()
	// 读取服务器配置
	serveConf := conf["server"].(map[interface{}]interface{})
	// 配置参数
	s := &http.Server{
		Addr:           fmt.Sprintf(":%v", serveConf["port"]),
		Handler:        r,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}

	// 测试服务连接
	r.GET("/ping", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"message": "pong",
		})
	})
	// 发布日志
	r.POST("/log/pub", pubLog)
	r.GET("/log/search", searchLog)

	// 检测，停止服务
	go graceful(s, 5*time.Second)
	// 配置生效，运行
	s.ListenAndServe()
}

// 停止服务
func graceful(hs *http.Server, timeout time.Duration) {
	stop := make(chan os.Signal, 1)

	signal.Notify(stop, os.Interrupt, syscall.SIGTERM)

	<-stop

	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()

	fmt.Println("Shutdown with timeout")

	if err := hs.Shutdown(ctx); err != nil {
		fmt.Println("Error: %v\n" + err.Error())
	} else {
		fmt.Println("Server stopped")
	}
}

// 创建日志
// @topic 主题，通常是项目名称
// @contents 日志内容 json, key:value
// @level ERROR > WARNING > INFO > DEBUG > TRACE
// @source IP
// @os {name, ver}
func pubLog(c *gin.Context) {
	// 参数体
	type log struct {
		Topic    string `form:"topic" json:"topic"`
		Contents string `form:"contents" json:"contents"`
	}
	// 解析参数
	var params log
	err := c.BindJSON(&params)
	if err != nil {
		fmt.Println(err.Error())
		c.JSON(500, err.Error())
		return
	}
	// nsq配置读取
	nsqConf := conf["nsq"].(map[interface{}]interface{})
	// 推送到消息队列
	config := nsq.NewConfig()
	w, _ := nsq.NewProducer(nsqConf["url"].(string), config)
	bodyJSON, _ := json.Marshal(params.Contents)
	err = w.Publish(params.Topic, bodyJSON)
	if err != nil {
		fmt.Println(err.Error())
		// 输出到消息队列
		c.JSON(500, err.Error())
		return
	}

	// 输出到消息队列
	c.JSON(200, "OK")
}

// 搜索日志
// @key 关键词
// 返回数据列表
func searchLog(c *gin.Context) {
	// 时间段参数
	fromD := c.Query("from_date")
	toD := c.Query("to_date")
	// 时间格式转换
	fromT, _ := time.Parse("2006-01-02", fromD)
	toT, _ := time.Parse("2006-01-02", toD)

	// 检测数据链接
	err := mgoSess.Ping()
	if err != nil {
		fmt.Println("MongoDB ping err: ", err.Error())
		c.JSON(500, err.Error())
		return
	}

	// 查询列表
	var list []gin.H
	// 连接数据
	s := mgoSess.Clone()
	defer s.Close()
	coll := s.DB(dbName).C("glog")
	// 时间段条件
	fromQ := bson.M{"time": bson.M{"&gte": fromT}}
	toQ := bson.M{"time": bson.M{"&lte": toT}}
	coll.Find(bson.M{"&and": []bson.M{fromQ, toQ}}).Skip(0).Limit(20).All(&list)

	fmt.Println(list)

	// 输出到消息队列
	c.JSON(200, list)
}
