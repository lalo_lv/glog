package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"time"

	"gopkg.in/mgo.v2"

	"github.com/gin-gonic/gin"
	nsq "github.com/nsqio/go-nsq"
	yaml "gopkg.in/yaml.v2"
)

var conf gin.H
var mgoSess *mgo.Session

// 库名
const dbName string = "dcj_common"

// Handle 消息处理
type Handle struct {
	msgchan chan *nsq.Message
	stop    bool
}

func main() {
	// 读取配置文件
	confile := flag.String("c", "config.yaml", "config file path")
	flag.Parse()
	buf, err := ioutil.ReadFile(*confile)
	if err != nil {
		fmt.Fprintf(os.Stderr, "File Error: %s\n", err)
		panic(err.Error())
	}
	// 解析 YAML 格式
	err = yaml.Unmarshal(buf, &conf)
	if err != nil {
		fmt.Println("YAML 解析错误")
		panic(err.Error())
	}

	// 连接数据库
	mgoSess, err = mgo.Dial(conf["mgo"].(string))
	if err != nil {
		fmt.Println("MongoDB连接失败")
		panic(err) // no, not really
	}
	mgoSess.SetMode(mgo.Monotonic, true)
	fmt.Println("Connected to MongoDB!")

	// 连接NSQ服务
	nsqConf := conf["nsq"].(map[interface{}]interface{})
	fmt.Println("连接NSQ服务 ...")
	cfg := nsq.NewConfig()
	consumer, err := nsq.NewConsumer(nsqConf["topic"].(string), nsqConf["channel"].(string), cfg)
	if err != nil {
		panic(err)
	}

	// 处理消息
	h := new(Handle)
	consumer.AddHandler(nsq.HandlerFunc(h.HandleMsg))
	h.msgchan = make(chan *nsq.Message, 1024)
	err = consumer.ConnectToNSQD(nsqConf["url"].(string))
	if err != nil {
		panic(err)
	}
	h.Process()
}

// HandleMsg 处理消息
func (h *Handle) HandleMsg(m *nsq.Message) error {
	if !h.stop {
		h.msgchan <- m
	}
	return nil
}

// Process 处理消息数据
func (h *Handle) Process() {
	fmt.Println("处理数据 ...")
	h.stop = false

	for {
		select {

		case m := <-h.msgchan:
			// 检测数据链接
			err := mgoSess.Ping()
			if err != nil {
				fmt.Println("MongoDB ping err: ", err.Error())
				h.stop = true
				break
			}

			var msgBody interface{}
			json.Unmarshal(m.Body, &msgBody)
			fmt.Println(msgBody)

			// 写入数据库
			s := mgoSess.Clone()
			defer s.Close()
			c := s.DB(dbName).C("glog")
			// 数据结构
			data := gin.H{
				"time":     time.Now().Unix(),
				"source":   "127.0.0.1",
				"contents": msgBody,
			}
			// insert data
			err = c.Insert(data)
			if err != nil {
				fmt.Println(err.Error())
			}

		case <-time.After(time.Second):
			if h.stop {
				close(h.msgchan)
				return
			}

		}
	}
}
